﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    class OpenFile
    {
        public OpenFileDialog dlg = new OpenFileDialog();
        public string Filenamereturn()
        {
            dlg.FileName = "Настройки сайта Т2"; // Default file name
            dlg.DefaultExt = ".txt"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                // Open document
                return dlg.FileName;
            }
            else
                return "can not open file";
        }
    }
}
