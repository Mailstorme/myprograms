﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form2 : Form
    {
        bool Ch;


        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            textBox1.Text = Properties.Settings.Default.GenAdress;
            Ch = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Ch = true;
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Ch)
                return;

            Properties.Settings.Default.GenAdress = textBox1.Text;
            Properties.Settings.Default.Save();
            MessageBox.Show("Внесенные изменения сохранены");
            
        }
    }
}
