﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Text;
using System.IO;
using mshtml;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading;

using Excel = Microsoft.Office.Interop.Excel;


namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        DateTime thisDay;
        bool PDFprinted = false;
        bool enter1 = false;
        bool GlobPause = false;
        int N; // Количество договоров
        int M; // Количестсво Атрибутов (изменится для Т2) + number
        string[][] arr;
        string[] t2id;
        string[] agents;
        StreamWriter sw;
        StreamWriter sw2;
        string RegAdress = "192.168.0.118/printing/index.php";
        List<string> ErList;
        List<string> TrueList;
        //string[] lines = { "First line", "Second line", "Third line" };
        System.IO.StreamWriter textFile;
        public string[] R1C1;


        public void arrcreate() // Функция заполняет массив arr используя генерацию людей
        {
            R1C1 = new string[] { "0", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ", "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ", "DA", "DB", "DC", "DD", "DE", "DF", "DG", "DH", "DI", "DJ", "DK", "DL", "DM", "DN", "DO", "DP", "DQ", "DR", "DS", "DT", "DU", "DV", "DW", "DX", "DY", "DZ", "EA", "EB", "EC", "ED", "EE", "EF", "EG", "EH", "EI", "EJ", "EK", "EL", "EM", "EN", "EO", "EP", "EQ", "ER", "ES", "ET", "EU", "EV", "EW", "EX", "EY", "EZ" };
            foreach (HtmlElement he in webBrowser2.Document.GetElementsByTagName("div"))
            {
                if (he.GetAttribute("className").Contains("fon_beeline"))  // Проверить имя класса для Т2
                {
                    N++;
                }
            }

            arr = new string[N+1][];   // Основной массив значений генерации
            arr[0] = new string[] {"bee_kod", "bee_family","bee_name", "bee_otchestvo","bee_date_rogden","bee_mesto_rogden",
            "bee_check_pol","bee_pol_g","country","bee_pasport_seria","bee_pasport_nomer","bee_pasport_vidan","bee_pasport_vidan_date"
            ,"bee_city_gorod","bee_city_street","bee_city_dom", "bee_city_korpus","bee_city_kvartira","bee_dataimesto","MESTO ZAKL DOG",
            "bee_number","bee_simcard","bee_tarif" }; // Атрибуты для вставки в форму, совпадают с классами в webbrowser2
            M = arr[0].Length;
            int k = 0;
            /*
            t2id = new string[] {  "d1",        "r1",      "r1",       "r1",           "w1",             "42",
            "82",           "82",        "43",       "d3",            "h3",                "p3",               "t3",
              "d4",            "h4",            "l4",            "l4",             "l4",                "v6",           "77" };
              */
            
            foreach (HtmlElement he in webBrowser2.Document.GetElementsByTagName("div")) // Создание Массива 
            {
                if (he.GetAttribute("className").Contains("fon_beeline"))
                {
                    k++;
                    arr[k] = new string[M];
                }
                for (int i = 0; i < M; i++)
                    if (he.GetAttribute("className") == (arr[0][i]))
                    {
                        arr[k][i] = he.InnerText;
                        break;
                    }
                if ((arr[k][3] == "undefined") || (arr[k][3] == null))
                {
                    arr[k][3] = "";
                }
            }
        }


        public Form1()
        {
            InitializeComponent();
            RegAdress = Properties.Settings.Default.GenAdress;
            webBrowser2.Navigate(RegAdress);                            // переход на страницу генерации
            webBrowser1.Navigate("https://www.t2wd.tele2.ru/t2wd/base3.zul?page=erf_edit");      // страница Т2 активации


            string T2idPath = Properties.Settings.Default.GlobPath;                              // путь к настройкам сайта т2 (поля формы  id)
            FileInfo T2idcheck = new FileInfo(T2idPath);

            string Agentpath = Properties.Settings.Default.AgentPath;                            // путь к логинапм и паролям агентов активации
            FileInfo Agentcheck = new FileInfo(Agentpath);

            int m = 0;
            int y = 0;
            int T2idCount = 0;
            int agentscount = 0;
            bool CorrectFile = false;
            bool CorrectFileAgent = false;


            #region получение данных из файла настройки сайта
            if (!T2idcheck.Exists)
            {
                OpenFile a = new OpenFile();
                T2idPath = a.Filenamereturn();
                //Properties.Settings.Default.GlobPath = T2idPath;
                //Properties.Settings.Default.Save();
            }

            while (!CorrectFile)
            {
                string[] Checklines = System.IO.File.ReadAllLines(T2idPath);

                if (!Checklines[0].Contains("T2 settings"))
                {
                    MessageBox.Show("Неверно указан файл с настройками сайта Т2");
                    OpenFile b = new OpenFile();
                    T2idPath = b.Filenamereturn();
                }
                else
                {
                    CorrectFile = true;
                }
            }

            string[] lines = System.IO.File.ReadAllLines(T2idPath);
            Properties.Settings.Default.GlobPath = T2idPath;
            Properties.Settings.Default.Save();

            foreach (string line in lines)
            {
                if (!line.Contains("\\"))
                {
                    T2idCount++;
                }
            }

            t2id = new string[T2idCount];
            
            foreach (string line in lines)
            {
                if (!line.Contains("\\"))
                {
                    t2id[m] = line.Substring(line.LastIndexOf('\"') - 2, 2);
                    m++;
                }
            }
            #endregion


            #region получение данных из файла информации об агентах
            if (!Agentcheck.Exists)
            {
                OpenFile2 a = new OpenFile2();
                Agentpath = a.Filenamereturn();
            }

            while (!CorrectFileAgent)
            {
                string[] Checklines = System.IO.File.ReadAllLines(Agentpath);

                if (!Checklines[0].Contains("Agents"))
                {
                    MessageBox.Show("Неверно указан файл с Агентами");
                    OpenFile2 b = new OpenFile2();
                    Agentpath = b.Filenamereturn();
                }
                else
                {
                    CorrectFileAgent = true;
                }
            }
            
            string[] text = System.IO.File.ReadAllLines(Agentpath);
            Properties.Settings.Default.AgentPath = Agentpath;
            Properties.Settings.Default.Save();
           
            foreach (string line in text)
            {
                if (!line.Contains("\\"))
                {
                    agentscount++;
                }
            }
            agents = new string[agentscount];
            foreach (string line in text)
            {
                if (!line.Contains("\\"))
                {
                    comboBox1.Items.Add(line.Substring(0,line.IndexOf(' ')));
                    agents[y] = line;
                    y++;
                   // MessageBox.Show(y.ToString());
                }
            }
#endregion
        }

        private void pause(int sleep)   // Пауза на 0,001 сек при sleep=1
        {
            double b = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while ((DateTime.Now.TimeOfDay.TotalMilliseconds - b) < sleep)
                Application.DoEvents();
        }


        public bool SavePdf(string Number)        // Сохранение контракта из webbrowser3, при успешном сохранении возвращает true паузы строго фиксированы и увеличены в W10 версии
        {
            string number = Number;
            int PDFtry = 0;
            string path = @"C:\T2_прога\" + thisDay.ToString("d") + "\\" +"PDF" + number + ".pdf";
            bool f = true;

            while (true)
            {
                webBrowser3.ShowSaveAsDialog();

                richTextBox1.AppendText(" 1 ");
                //SendKeys.SendWait("{ENTER}");       // Сохранение PDF
                //pause(7000);
                if (f)
                    pause(500);

                SendKeys.SendWait("1");
                SendKeys.SendWait("{BS}");

                if (f)
                    pause(400);
                else
                    pause(100);
                SendKeys.SendWait(@"C:\T2_прога\" + thisDay.ToString("d") + "\\" + "PDF" + number + ".pdf");

                if (f)
                    pause(400);
                else
                    pause(100);

                SendKeys.SendWait("{ENTER}");
                if (f)
                    pause(700);
                SendKeys.SendWait("{ENTER}");
                if (f)
                    pause(200);

                if (!f)
                {
                    f = true;
                    pause(2000);
                    richTextBox1.AppendText(" Y ");
                    continue;
                }

                PDFtry++;

                FileInfo PDFcheck = new FileInfo(path);
                if (PDFcheck.Exists)
                {
                    if (PDFcheck.Length > 15000)
                    {
                        return true;
                    }
                    else
                    {
                        PDFcheck.Delete();
                        //pause(2000);
                        pause(1000);
                    }
                }
                else
                {
                    pause(2000);
                    //richTextBox1.AppendText(" ! ");
                    //richTextBox1.AppendText("");
                }
                          

                if (PDFtry > 2)
                    return false;
            }
        }


        public void PressCancel(string str, int i)  // нажатие кнопки отмена при ошибке заполнения, сохранения, загрузки и тд, и вывод сообщения str в richtextbox
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button")) // нажимаем "Отмена"
            {
                if (he.GetAttribute("id").Contains("z8"))
                {
                    he.InvokeMember("click");
                    richTextBox1.AppendText(str + "\n");
                    while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                        Application.DoEvents();
                }
            }
        }


        public bool LoadWait(WebBrowser browser, int t, int i) // ожидает загрузки страницы указанного вебброузера
        {
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (true)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < t * 1000)
                {
                    if (browser.Url != null)
                        return true;
                    Application.DoEvents();
                }
                else
                {
                    AddEr(i, "Ошибка загрузки webbrowser (8) для номера -- " + arr[i][M - 3] + "\n");

                    richTextBox1.AppendText("Ошибка загрузки webbrowser (8) для номера -- " + arr[i][M - 3] + "\n");
                    return false;
                }
            }
        }


        public void AddEr(int i, string ErText)  // запись ошибки ErText в файл, i для указания элемента
        {
            ErList.Add(ErText);
            sw = new StreamWriter(@"C:\T2_прога\" + thisDay.ToString("d") + "\\" +"Errors.txt", true);
            sw.WriteLine(ErText);
            sw.Close();
        }



        public void AddToExcel(int i, string f2)  // делает запись в Excel после каждого зарегестрированного номера
        {
            var ExcelApp = new Excel.Application();
            ExcelApp.Visible = false;
            Excel.Sheets excelsheets;
            Excel.Worksheet excelworksheet;
            Excel.Workbooks workbooks;
            Excel.Workbook book;
            Excel.Range range;
            FileInfo Excel1 = new FileInfo(f2);
            if (!Excel1.Exists)
            {
                book = ExcelApp.Workbooks.Add(System.Reflection.Missing.Value);
            }
            else
            {
                book = ExcelApp.Workbooks.Open(@f2, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }

            //workbooks = ExcelApp.Workbooks;
            excelsheets = book.Worksheets;
            excelworksheet = (Excel.Worksheet)excelsheets.get_Item(1);

            //string p1 = R1C1[i];
            int rows = book.ActiveSheet.UsedRange.Rows.Count;

            if (!Excel1.Exists)
            {

            }
            else
            {
                rows++;
            }



            string p1 = rows.ToString();
            string p2 = R1C1[M];
            range = excelworksheet.get_Range("A" + p1 + ":" + p2 + p1);

            object[] obarr = new object[M];

            obarr[0] = arr[i][M - 3];
            obarr[1] = "'" + arr[i][M - 2];
            obarr[2] = arr[i][M - 1];

            for (int j = 3; j < M; j++)
            {
                obarr[j] = arr[i][j - 3];
            }

            range.Value2 = obarr;

            if (!Excel1.Exists)
            {
                book.SaveAs(f2);
            }
            else
            {
                book.Save();
            }
            book.Close(0);
            workbooks = ExcelApp.Workbooks;
            if (workbooks.Count == 0)
                ExcelApp.Quit();
            ExcelApp.Quit();
            /*
            ExcelApp = null;
            excelsheets = null;
            excelworksheet = null;
            workbooks = null;
            book = null;
            range = null; 
            */
            System.Runtime.InteropServices.Marshal.ReleaseComObject(ExcelApp);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelsheets);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelworksheet);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(book);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(range);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(workbooks);

            ExcelApp = null;
            excelsheets = null;
            excelworksheet = null;
            workbooks = null;
            book = null;
            range = null;
        }



        public bool PingGoogle(int t) // пингует гугл для проверки соединения
        {

            try
            {
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply pingReply = ping.Send("8.8.8.8.");

                double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
                while (pingReply.Status.ToString() != "Success")
                {
                    if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < t * 1000)
                    {
                        pingReply = ping.Send("8.8.8.8.");
                    }
                    else
                    {
                        // AddEr(i, ErText + " -- " + arr[i][M - 3] + "\n");
                        // richTextBox1.AppendText(ErText + " -- " + arr[i][M - 3] + "\n");
                        break;
                    }
                }
                if (pingReply.Status.ToString() == "Success")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception er)
            {
                richTextBox1.AppendText( "ошибка при Пинге: " + er.Message + "/n");
                return false;
            }
        }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    private void button1_Click(object sender, EventArgs e)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("INPUT"))  // Проверка входа на сайт Т2
            {
                if (he.GetAttribute("title") == "Номер телефона полностью (10 цифр), без префикса 8 или +7")
                {
                    enter1 = true;
                    break;
                }
            }

            bool enter2 = false;
            foreach (HtmlElement he in webBrowser2.Document.GetElementsByTagName("div"))  // Проверка на Генерацию 
            {
                if (he.GetAttribute("className").Contains("fon_beeline"))
                {
                    enter2 = true;
                    break;
                }
            }



            if (enter1 && enter2)  // Если мы вошли и скенерировали список людей
            {
                thisDay = DateTime.Today;
                string excelfile = System.DateTime.Now.ToString();
                excelfile = excelfile.Replace(':', '-');
                Directory.CreateDirectory(@"C:\T2_прога\" + thisDay.ToString("d") + "\\" +"Excel\\");   // Создание дерева папок для результатов
                N = 0;
                M = 0;
                sw = new StreamWriter(@"C:\T2_прога\" + thisDay.ToString("d") + "\\" +"Errors.txt", true);   // инициализация txt файлов
                string Date = DateTime.Now.ToString("dd MMMM yyyy | HH:mm:ss");
                sw.WriteLine("Ошибки за " + Date);
                sw.Close();

                sw2 = new StreamWriter(@"C:\T2_прога\" + thisDay.ToString("d") + "\\" +"Отправленные номера.txt", true);
                sw2.WriteLine("Отправленные номера за " + Date);
                sw2.Close();

                textFile = new System.IO.StreamWriter(@"C:\T2_прога\" + thisDay.ToString("d") + "\\" +"Сгенерированный список.txt", true);
                ErList = new List<string>();
                TrueList = new List<string>();

                arrcreate();  // генерируем массив
                int[] TryCount = new int[N + 1];
                for (int j = 0; j < N + 1; j++)
                {
                    TryCount[j] = 0;
                }

                string wb2list = webBrowser2.DocumentText;


                string PDFNumber = "";

                Encoding encoding = Encoding.GetEncoding(1251);
                //string htmlko = webBrowser2.DocumentText;
                Stream stream = webBrowser2.DocumentStream;
                StreamReader sr = new StreamReader(stream, encoding);
                wb2list = sr.ReadToEnd();
                stream.Close();
                textFile.Write(wb2list);                             // Сохраняем Сгенерированный список на всякий случай
                textFile.Close();


                while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                    Application.DoEvents();

                pause(1500);

                //////////////////////////////////////////////
                //////////////////////////////////////////////   ЦИКЛ активации номеров
                //////////////////////////////////////////////

                for (int i = 1; i <= N; i++)
                {
                    if (GlobPause)
                    {
                        richTextBox3.Text = "Пауза";
                        while (true)
                        {
                            if (!GlobPause)
                            {
                                break;
                            }
                            pause(500);
                        }
                    }
                    richTextBox3.Text = "Программа работает";

                    PDFprinted = false;
                    bool pointcorrect = true;
                    pause(500);
                    richTextBox2.Text = i.ToString() + " из " + N.ToString();
                    //pause(2000);

                    if (!LoadWait(webBrowser1, 10, i))
                    {
                        continue;
                    }


                    bool polenomera = false;
                    bool navigate = false;
                    double curtime1 = DateTime.Now.TimeOfDay.TotalMilliseconds;
                    double curtimeSek = DateTime.Now.TimeOfDay.TotalSeconds;

                    while (true)
                    {
                        if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime1) < 18 * 1000) // Ожидаем загрузку страницы активации и ввод номера и ссим
                        {

                            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("INPUT"))  // Ввод номера и сим
                            {

                                if (he.GetAttribute("title") == "Номер телефона полностью (10 цифр), без префикса 8 или +7")
                                {
                                    polenomera = true;
                                    he.InvokeMember("focus");
                                    pause(500);
                                    he.SetAttribute("value", arr[i][M - 3].Remove(0, 1));
                                }
                                if (he.GetAttribute("title") == "Cерийный номер SIM-карты целиком (20 цифр) или последние 5 цифр, включая последнюю контрольную")
                                {
                                    he.InvokeMember("focus");
                                    he.SetAttribute("value", arr[i][M - 2]);
                                }
                            }
                            if (polenomera)
                                break;
                            else
                                pause(200);
                        }
                        else
                        {
                            webBrowser1.Navigate("https://www.t2wd.tele2.ru/t2wd/base3.zul?page=erf_create");
                            navigate = true;
                            break;
                        }
                    }
                    if (navigate)
                    {
                        if (TryCount[i] < 2)
                        {
                            AddEr(i, "Ошибка загрузки страницы (11+), отправили ещё раз номер -- " + arr[i][M - 3] + "\n");
                            richTextBox1.AppendText("Ошибка загрузки страницы (11+), отправили ещё раз номер -- " + arr[i][M - 3] + "\n");
                            TryCount[i]++;
                            i--;
                            continue;
                        }
                        else
                        {
                            AddEr(i, "Ошибка загрузки страницы (11) для номера -- " + arr[i][M - 3] + "\n");
                            richTextBox1.AppendText("Ошибка загрузки страницы (11) для номера -- " + arr[i][M - 3] + "\n");
                            continue;
                        }
                    }
                    pause(200);


                    SendKeys.SendWait("{TAB}");  // после таба может появиться ошибка, которую мы проверяем далее

                    //pause(1500);
                    pause(1000);

                    bool errinenter = true;
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("div"))   // проверка контента на ошибки
                    {
                        if (he.GetAttribute("className").Contains("errbox"))
                        {
                            AddEr(i, "Неверный ICCID или номер (6) для номера -- " + arr[i][M - 3] + "\n");

                            richTextBox1.AppendText("Неверный ICCID или номер (6) для номера -- " + arr[i][M - 3] + "\n");
                            errinenter = false;
                            break;
                        }
                    }
                    if (!errinenter)
                        continue;

                    //pause(1500);


                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button")) // нажимаем "Искать"
                    {
                        if (he.OuterHtml.Contains("dblClickProtected"))
                        {
                            //he.InvokeMember("click");
                            he.InvokeMember("focus");
                            SendKeys.SendWait("{ENTER}");
                            break;
                        }
                    }


                    while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                        Application.DoEvents();

                    //pause(1000);
                    pause(50);

                    if (!LoadWait(webBrowser1,10,i))
                    {
                        continue;
                    }


                    ////// https://www.t2wd.tele2.ru/t2wd/base3.zul?page=erf_edit
                    ////// https://www.t2wd.tele2.ru/t2wd/base3.zul?page=erf_create


                    bool FormPage1 = false;
                    bool FormPage2 = false;
                    bool FormPage3 = false;
                    bool PopMes = false;
                    bool TimeEnd = false;
                    double curtime3 = DateTime.Now.TimeOfDay.TotalMilliseconds;

                    #region цикл ожидания загрузки страницы формы, выходим когда появляются поля для заполнения и проверяем всплывающие окна с огшибками
                    while (true)  // цикл ожидания загрузки страницы формы, выходим когда появляются поля для заполнения и проверяем всплывающие окна с огшибками
                    {
                        if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime3) < 12 * 1000)
                        {
                            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("INPUT"))
                            {
                                if (he.GetAttribute("id").Contains("d1"))
                                {
                                    FormPage1 = true;
                                }
                                if (he.GetAttribute("id").Contains("r1"))
                                {
                                    FormPage2 = true;
                                }
                                if (he.GetAttribute("id").Contains("w1"))
                                {
                                    FormPage3 = true;
                                }
                            }

                            if ((FormPage1 && FormPage2 && FormPage3))  // Если мы оказались на форме активации то выходим из цикла
                            {
                                break;
                            }

                            // PopMes
                            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("span")) // Проверка Всплывающего сообщения (может выскочить после нажати "искать")
                            {
                                if (he.OuterHtml.Contains("Комплект подключения не найден. Попробуйте уточнить параметры поиска."))
                                {
                                    AddEr(i, "Комплект подключения не найден (5) для номера -- " + arr[i][M - 3] + "\n");
                                    richTextBox1.AppendText("Комплект подключения не найден (5) для номера -- " + arr[i][M - 3] + "\n");

                                    foreach (HtmlElement hh in webBrowser1.Document.GetElementsByTagName("button")) // нажимаем "Ок"
                                        if (hh.GetAttribute("id").Contains("b1"))
                                        {
                                            hh.InvokeMember("click");
                                            pause(100);
                                        }
                                    PopMes = true;
                                    break;
                                }
                                else if (he.OuterHtml.Contains("Для найденного объекта создание ЭРФ невозможно:"))
                                {
                                    AddEr(i, "Контракт уже заключен для номера -- " + arr[i][M - 3] + "\n");
                                    richTextBox1.AppendText("Контракт уже заключен для номера -- " + arr[i][M - 3] + "\n");

                                    foreach (HtmlElement hh in webBrowser1.Document.GetElementsByTagName("button")) // нажимаем "Ок"
                                        if (hh.GetAttribute("id").Contains("b1"))
                                        {
                                            hh.InvokeMember("click");
                                            pause(100);
                                        }
                                    PopMes = true;
                                    break;
                                }
                                else if (he.OuterHtml.Contains("Для этого комплекта подключения уже создана ЭРФ."))
                                {
                                    AddEr(i, "Для этого комплекта подключения уже создана ЭРФ -- " + arr[i][M - 3] + "\n");
                                    richTextBox1.AppendText("Для этого комплекта подключения уже создана ЭРФ -- " + arr[i][M - 3] + "\n");

                                    foreach (HtmlElement hh in webBrowser1.Document.GetElementsByTagName("button")) // нажимаем "Ок"
                                        if (hh.GetAttribute("id").Contains("b1"))
                                        {
                                            hh.InvokeMember("click");
                                            pause(100);
                                        }
                                    PopMes = true;
                                    break;
                                }
                            }

                            if (PopMes)
                            {
                                break;
                            }

                            pause(150);
                        }
                        else
                        {
                            TimeEnd = true;
                            break;
                        }
                    }

                    if (TimeEnd)
                    {
                        if (TryCount[i] < 2)
                        {
                            AddEr(i, "Ошибка загрузки формы (13+), отправили ещё раз номер -- " + arr[i][M - 3] + "\n");
                            richTextBox1.AppendText("Ошибка загрузки страницы (13+), отправили ещё раз номер -- " + arr[i][M - 3] + "\n");
                            TryCount[i]++;
                            i--;
                            continue;
                        }
                        else
                        {
                            AddEr(i, "Ошибка загрузки формы (13) для номера -- " + arr[i][M - 3] + "\n");
                            richTextBox1.AppendText("Ошибка загрузки страницы (13) для номера -- " + arr[i][M - 3] + "\n");
                            continue;
                        }
                    }

                    if (PopMes) // если вылезло одно из всплывающих окон то переходим к след номеру
                    {
                        continue;
                    }

                    pause(50);
                    #endregion

                    #region Адрес может быть уже заполнен, такой контракт нужно заполнить вручную (делается это после активации остальных номеров)
                    bool EmptyAdress = true;
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("INPUT"))      // Проверка на заполненный адрес
                    {
                        if (he.GetAttribute("id").Contains("b5") || he.GetAttribute("id").Contains("75") || he.GetAttribute("id").Contains("06") || he.GetAttribute("id").Contains("d6"))
                        {
                            if (he.GetAttribute("value") != "")
                            {
                                EmptyAdress = false;
                                AddEr(i, "Адрес доставки или конт. инф. заполнены оператором, введите этот номер вручную -- " + arr[i][M - 3] + "\n");
                                break;
                            }
                        }
                    }
                    if (!EmptyAdress)
                    {
                        PressCancel("Адрес доставки или конт. инф. заполнены оператором, введите этот номер вручную -- " + arr[i][M - 3] + "\n", i);
                        continue;
                    }
                    #endregion

                    #region выбираем значения во всех выпадающих списках формы
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("SELECT")) // выбираем значения во всех выпадающих списках формы
                    {
                        if (he.GetAttribute("id").Contains("83"))
                        {
                            //he.InvokeMember("onchange");
                            //he.SetAttribute("value", "Паспорт иностранного гражданина (национальный или заграничный)");

                            he.InvokeMember("click");
                            //pause(500);
                            pause(150);
                            he.SetAttribute("selectedindex", "4");
                            //pause(500);
                            pause(150);
                            he.Children[4].SetAttribute("selected", "selected");
                            //pause(500);
                            pause(150);
                            he.InvokeMember("click");
                        }
                        else if (he.GetAttribute("id").Contains("82"))
                        {
                            he.InvokeMember("click");
                            //pause(1000);
                            pause(300);
                            if (arr[i][6].Contains("m"))
                                he.Children[1].SetAttribute("selected", "selected");
                            else
                                he.Children[2].SetAttribute("selected", "selected");
                            //pause(500);
                            pause(150);
                            he.InvokeMember("click");
                        }
                    }
                    #endregion

                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("INPUT")) // заполнение основных input-ов
                    {
                        for (int j = 0; j < M - 3; j++)                          // перебор тегов из t2id (массив взятый из файда настроек, порядок такой же как и в массиве arr) 
                        {
                            if (he.GetAttribute("id").Contains(t2id[j]))
                                if (t2id[j] == "d1")                             // Проверка на КОД ТОЧКИ
                                {
                                    he.InvokeMember("focus");
                                    //pause(500);
                                    pause(150);
                                    he.SetAttribute("value", arr[i][0]);


                                    //pause(500);
                                    pause(150);

                                    SendKeys.SendWait("{TAB}");



                                    double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
                                    bool TimeInTT = false;
                                    bool find = false;

                                    while (true)
                                    {
                                        if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < 12 * 1000)  // после заполнения кода ТТ при правильной ТТ должно появиться всплывающее окно с кнопкой "ок" на которую  следует нажать
                                        {
                                            foreach (HtmlElement hh in webBrowser1.Document.GetElementsByTagName("button")) // нажимаем "Ок"
                                            {
                                                if (hh.GetAttribute("id").Contains("_a"))  //
                                                {
                                                    hh.InvokeMember("click");
                                                    pause(1000);
                                                    find = true;
                                                    break;
                                                    //he.InvokeMember("focus");
                                                    //SendKeys.SendWait("{ENTER}");
                                                }
                                                else 
                                                    if (hh.GetAttribute("id").Contains("_z_")) // Ситуация при неверном коде ТТ, появится окно с id кнопки _z_
                                                {
                                                    pointcorrect = false;
                                                    AddEr(i, "Не найдена ТТ для номера -- " + arr[i][M - 3] + "\n");

                                                    richTextBox1.AppendText("Не найдена ТТ для номера -- " + arr[i][M - 3] + "\n");
                                                    hh.InvokeMember("click");
                                                    pause(1000);
                                                    find = true;
                                                    break;
                                                }
                                            }
                                            if (find)
                                                break;
                                            Application.DoEvents();
                                        }
                                        else
                                        {
                                            AddEr(i, "Ошибка при вводе ТТ (9) для номера -- " + arr[i][M - 3] + "\n");

                                            richTextBox1.AppendText("Ошибка при вводе ТТ (9) для номера -- " + arr[i][M - 3] + "\n");
                                            TimeInTT = true;
                                            break;
                                        }
                                    }

                                    if (TimeInTT)
                                    {
                                        pointcorrect = false;
                                    }
                                }

                                else
                                if (t2id[j] == "r1")  // заполнение ФИО
                                {
                                    he.InvokeMember("focus");
                                    he.SetAttribute("value", arr[i][1].Replace(" ", "") + " " + arr[i][2].Replace(" ", "") + " " + arr[i][3].Replace(" ", ""));  //  Проверить позиции фамилии имени и отчества в arr[0][]
                                    //SendKeys.SendWait("{TAB}");
                                    pause(200);
                                    break;
                                }
                                else if (t2id[j] == "w1" || t2id[j] == "t3")  // || t2id[j] == "v6" для даты закл договора
                                {
                                    he.InvokeMember("focus");
                                    he.SetAttribute("value", (arr[i][j]).Replace(' ', '.'));
                                    pause(200);
                                    break;
                                }
                                
                                else if (t2id[j] == "v6")  // 
                                {
                                    he.InvokeMember("focus");
                                    he.SetAttribute("value", (arr[i][j]).Replace(' ', '.'));
                                    //SendKeys.SendWait("{TAB}");
                                    pause(200);
                                    break;
                                }
                                
                                else if (t2id[j] == "42" || t2id[j] == "43" || t2id[j] == "d3" || t2id[j] == "h3" || t2id[j] == "p3" || t2id[j] == "d4" || t2id[j] == "h4")
                                {
                                    he.InvokeMember("focus");
                                    he.SetAttribute("value", (arr[i][j]));
                                    pause(200);
                                    break;
                                }
                                else if (t2id[j] == "l4") // дом,корпус,квартира
                                {
                                    he.InvokeMember("focus");
                                    he.SetAttribute("value", "д." + arr[i][j] + " к." + arr[i][j + 1] + " " + arr[i][j + 2]);
                                    pause(200);
                                    break;
                                }
                        }
                        if (!pointcorrect)
                            break;
                    }


                    if (!pointcorrect)   // Выход Если Неверный Код ТТ
                    {
                        foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button")) // нажимаем "Отмена"
                        {
                            if (he.GetAttribute("id").Contains("z8"))
                            {
                                he.InvokeMember("click");
                                while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                                    Application.DoEvents();
                                break;
                            }
                        }
                        continue;
                    }


                    //pause(1500);
                    pause(200);

                    SendKeys.SendWait("{TAB}");
                    pause(200);

                    
                    string p1 = "";
                    string p2 = "";
                    bool Mesto = true;
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("INPUT"))   // проверка контента на правильность заполнения
                    {

                        for (int j = 0; j < M - 3; j++)
                        {
                            if (he.GetAttribute("id").Contains(t2id[j]))
                            {
                                if (t2id[j] == "d4")
                                    p1 = he.GetAttribute("value");
                                else if (t2id[j] == "h4")
                                {
                                    p2 = he.GetAttribute("value");
                                }
                                else if (t2id[j] == "77")
                                    if (he.GetAttribute("value") == "")                 // Место заключения договора заполняется после выбора ТТ
                                    {
                                        Mesto = false;
                                        break;
                                    }
                            }
                        }

                        if (he.GetAttribute("id").Contains("35"))
                        {
                            if (he.GetAttribute("value") != "")             // Поле доставки должно быть пустым
                            {
                                Mesto = false;
                            }
                        }
                        else if (he.GetAttribute("id").Contains("96"))
                        {
                            if (he.GetAttribute("value") != "")             // Поле факс должно быть пустым
                            {
                                Mesto = false;
                            }
                        }
                        else if (he.GetAttribute("id").Contains("x5"))
                        {
                            if (he.GetAttribute("value") != "")             // Поле кодовое слово должно быть пустым
                            {
                                Mesto = false;
                            }
                        }
                    }

                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("div"))   // проверка контента на errbox
                    {
                        if (he.GetAttribute("className").Contains("errbox"))
                        {
                            Mesto = false;
                        }
                    }


                    if ((p1 == p2) || (Mesto != true)) // реакция на ошибки в заполнении
                    {

                        if (TryCount[i] < 2)
                        {
                            AddEr(i, "Ошибка заполнения формы (4+), отправили ещё раз номер  -- " + arr[i][M - 3] + "\n");
                            PressCancel("Ошибка заполнения формы (4+), отправили ещё раз номер-- " + arr[i][M - 3], i);
                            TryCount[i]++;
                            i--;
                            continue;
                        }
                        else
                        {
                            AddEr(i, "Окончательная ошибка заполнения формы (4), для номера  -- " + arr[i][M - 3] + "\n");
                            PressCancel("Окончательная ошибка заполнения формы (4), для номера -- " + arr[i][M - 3], i);
                            continue;
                        }
                    }


                    pause(50);

                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button")) // нажимаем кнопку "Скопировать адрес места жительства"
                    {
                        if (he.GetAttribute("id").Contains("w4"))
                        {
                            he.InvokeMember("click");
                            while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                                Application.DoEvents();
                            break;
                        }
                    }


                    pause(500);
                    SendKeys.SendWait("{TAB}");
                    pause(100);
                    textBox1.Focus();
                    pause(100);

                    //richTextBox1.AppendText(" 1 ");
                    //webBrowser3.Navigate("https://www.google.ru/");
                    //pause(800);
                    webBrowser3.Navigate(RegAdress); // в wb3 открываются pdf файлы, но чтобы не сохранился предыдущий контракт на месте текущего переходим на левый сайт
                    pause(1000);

                    //richTextBox1.AppendText(" 2 ");

                    while (webBrowser3.ReadyState != WebBrowserReadyState.Complete)
                        Application.DoEvents();

                    bool justsaved = false;

                    //richTextBox1.AppendText(" 3 ");

                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button")) // нажимаем "Печать" откроется pdf файл в webbrowser3
                    {
                        //PDFNumber = arr[i][M-3].ToString();
                        if (he.GetAttribute("id").Contains("19"))
                        {
                            he.InvokeMember("click");
                            while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                                Application.DoEvents();
                            break;
                        }
                    }

                    //richTextBox1.AppendText(" 4 ");


                    //Получаем элемент head
                    HtmlElement head = webBrowser3.Document.GetElementsByTagName("head")[0];
                    //Создаем новый элемент, который будет содержать наш скрипт
                    HtmlElement script = webBrowser3.Document.CreateElement("script");
                    IHTMLScriptElement elementscript = (IHTMLScriptElement)script.DomElement;
                    //Записываем текст скрипта, который будет выполняться
                    //В нём мы переопределяем функции, с помощью которых появляются "Сообщения от страницы"
                    elementscript.text = "function blockAlerts() {alert = function(){};confirm = function(){};}";
                    //Добавляем скрипт на страницу
                    head.AppendChild(script);
                    //Вызываем нашу функцию blockAlerts, которую описали в скрипте
                    webBrowser3.Document.InvokeScript("blockAlerts");






                    if (!LoadWait(webBrowser3, 10, i))
                    {
                        continue;
                    }


                    pause(1700);
                    //pause(200);

                    PDFNumber = arr[i][M - 3].ToString();


                    // Сохраняем PDF

                    double curtime2 = DateTime.Now.TimeOfDay.TotalMilliseconds;
                    bool PdfError = false;
                    bool Erload = false;
                    while (true)
                    {
                        if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime2) < 6 * 1000)
                        {
                            if (webBrowser3.Url.ToString() == "https://www.t2wd.tele2.ru/t2wd/servlet/pdfgen")
                            {
                                if (SavePdf(PDFNumber + "-" + (i).ToString())) // сохраняем пдф
                                {
                                    label3.Text = PDFNumber + "-" + (i).ToString();
                                    justsaved = true;
                                }
                                else
                                {
                                    PdfError = true;
                                }
                                break;
                            }
                            else
                            {
                                pause(200);
                            }
                        }
                        else
                        {
                            Erload = true;
                            break;
                        }
                    }

                    if(PdfError)  // возможные ошибки
                    {
                        AddEr(i, "Ошибка PDF (1) номер не был внесён в базу -- " + arr[i][M - 3] + "\n");
                        PressCancel("Ошибка PDF (1) номер не был внесён в базу -- " + arr[i][M - 3], i);
                        continue;
                    }
                    if (Erload)
                    {
                        if (TryCount[i] < 2)
                        {
                            AddEr(i, "Ошибка перехода к PDF (7+), отправили ещё раз номер -- " + arr[i][M - 3] + "\n");
                            PressCancel("Ошибка перехода к PDF (7+), отправили ещё раз номер -- " + arr[i][M - 3], i);
                            TryCount[i]++;
                            i--;
                            continue;
                        }
                        else
                        {
                            AddEr(i, "Ошибка перехода к PDF (7) для номера -- " + arr[i][M - 3] + "\n");
                            PressCancel("Ошибка перехода к PDF (7) для номера -- " + arr[i][M - 3], i);
                            continue;
                        }
                    }

                     
                    string path = @"C:\T2_прога\" + thisDay.ToString("d") + "\\" +"PDF" + PDFNumber + "-" + (i).ToString() + ".pdf";
                    FileInfo PDFcheck = new FileInfo(path);  // проверяем сохраненный пдф файл
                    bool Send = false;

                    if (PDFcheck.Exists)
                    {
                        if (PDFcheck.Length > 20000)
                        {
                            //pause(1000);
                            pause(300);

                            if ((PDFprinted) && (justsaved))
                            {
                                if (PingGoogle(10))
                                {
                                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button"))   // Отправляем на проверку
                                    {
                                        if (he.GetAttribute("id").Contains("x8")) // нажатие кнопки и сохранение
                                        {
                                            he.InvokeMember("click");
                                            Send = true;
                                            TrueList.Add(arr[i][M - 3] + "\n");
                                            string s = (DateTime.Now.TimeOfDay.TotalSeconds - curtimeSek).ToString();
                                            s = s.Substring(0, s.LastIndexOf(',') + 2) + "сек";
                                            s = " " + s;
                                            sw2 = new StreamWriter(@"C:\T2_прога\" + thisDay.ToString("d") + "\\" + "Отправленные номера.txt", true);
                                            sw2.WriteLine(arr[i][M - 3] + s + "\n");
                                            sw2.Close();
                                            richTextBox1.AppendText("Номер -- " + arr[i][M - 3] + s + " отправлен на проверку \n");

                                            AddToExcel(i, @"C:\T2_прога\" + thisDay.ToString("d") + "\\" + "Excel\\Excel" + excelfile + ".xlsx");

                                            while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                                                Application.DoEvents();
                                            break;
                                        }
                                    }
                                }
                                else  // возможные ошибки 
                                {
                                    AddEr(i, "Ошибка отправки новера (12) пдф была получена, но номер не был отправлен -- " + arr[i][M - 3] + "\n");
                                    PressCancel("Ошибка отправки новера (12) пдф была получена, но номер не был отправлен -- " + arr[i][M - 3], i);
                                }
                                if (Send)
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                AddEr(i, "Ошибка сохранения PDF (10) номер не был внесён в базу -- " + arr[i][M - 3] + "\n");
                                PressCancel("Ошибка сохранения PDF (10) номер не был внесён в базу -- " + arr[i][M - 3], i);
                                PDFcheck.Delete();
                                pause(2000);
                                continue;
                            }     
                                                         
                        }
                        else
                        {
                            AddEr(i, "Ошибка PDF (2) номер не был внесён в базу -- " + arr[i][M - 3] + "\n");

                            PressCancel("Ошибка PDF (2) номер не был внесён в базу -- " + arr[i][M - 3], i);
                            PDFcheck.Delete();
                            pause(2000);
                            continue;
                        }
                    }
                    else
                    {
                        AddEr(i, "Ошибка PDF (3) номер не был внесён в базу -- " + arr[i][M - 3] + "\n");

                        PressCancel("Ошибка PDF (3) номер не был внесён в базу -- " + arr[i][M - 3], i);
                        pause(2000);
                        continue;
                    }


                    if (!Send) // если по какой-лио причине номер не отправился, то удаляем пдф и идём дальше
                    {
                        if (PDFcheck.Exists)
                        {
                            PDFcheck.Delete();
                        }
                        AddEr(i, "Непредвиденная ошибка с номером -- " + arr[i][M - 3] + "\n");
                        richTextBox1.AppendText("Непредвиденная ошибка с номером -- " + arr[i][M - 3] + "\n");
                        webBrowser1.Navigate("https://www.t2wd.tele2.ru/t2wd/base3.zul?page=erf_create");

                        if (!LoadWait(webBrowser1, 10, i))
                        {
                            while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                                Application.DoEvents();
                            pause(2000);
                            continue;
                        }
                    }

                    //pause(750);
                    pause(200);
                }



                ///////////////////////////////////////////////////////////  Завершающий этап вне цикла
                ///////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////



                richTextBox3.Text = "Программа завершила работу";

                if (TrueList.Count == N)
                {
                    MessageBox.Show("Программа завершила работу");
                }
                else
                {
                    MessageBox.Show("Программа завершила работу. Не все номера были зарегестрированны, откройте папку C:\\T2_прога за дополнительнйо информацией ");
                }   
            }
            else if (!enter1) 
                    MessageBox.Show("Авторизуйтесь на сайте");
                else if (!enter2)
                {
                    MessageBox.Show("Сгенерируйте номера");
                }
        }



        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void webBrowser2_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void webBrowser1_NewWindow_1(object sender, CancelEventArgs e)
        {
            //MessageBox.Show("Вошли в NewWINDOW");
            //((WebBrowser)sender).Url = new Uri(((WebBrowser)sender).StatusText);
            e.Cancel = true;
            PDFprinted = true;
            //MessageBox.Show("Сохранили");
            webBrowser3.Navigate("https://www.t2wd.tele2.ru/t2wd/servlet/pdfgen");
            
            /*
            pause(4000);
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("HTML"))
            {
                //MessageBox.Show("InnerHtml  -  " + he.InnerHtml);
                MessageBox.Show("OuterHtml  -  " + he.OuterHtml);
            }
            */
            /*
            HtmlElement link = webBrowser1.Document.ActiveElement;
            string url = link.GetAttribute("href");
            webBrowser1.Navigate(url);
            */
        }

        private void webBrowser3_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox1.ScrollToCaret();
        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox3_TextChanged(object sender, EventArgs e)
        {
            richTextBox3.ScrollToCaret();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (GlobPause)
            {
                richTextBox3.Text = "Программа работает";
                GlobPause = false;
                //MessageBox.Show(GlobPause.ToString());
            }
            else
            {
                richTextBox3.Text = "Дождитесь конца итерации цикла";
                GlobPause = true;
                //MessageBox.Show(GlobPause.ToString());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            webBrowser2.Navigate(RegAdress);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)  // ввод пароля и логина пользователя
        {
            int l = 0;
            for (int i = 0; i < agents.Count(); i++)
                if (agents[i].Contains(comboBox1.Text))
                {
                    l = i;
                    break;
                }
            string agentname = "";
            agentname = agents[l].Substring(comboBox1.Text.Length + 1, agents[l].LastIndexOf(' ')- comboBox1.Text.Length - 1);
            string agentpass = agents[l].Substring(agents[l].LastIndexOf(' ')+1);

            bool enter = false;
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("INPUT"))        // Вход в Т2
            {
                if (he.GetAttribute("name") == "j_username")
                {
                    he.SetAttribute("value", agentname);
                    enter = true;
                }
                if (he.GetAttribute("name") == "j_password")
                    he.SetAttribute("value", agentpass);
            }

            if (enter)
                foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("INPUT"))
                    if (he.GetAttribute("value") == "Войти")
                        he.InvokeMember("click");

            while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                Application.DoEvents();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            for (int i = 1; i < 20; i++)
            {
                SendKeys.SendWait(i.ToString());
                pause(500);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                //MessageBox.Show("покапока");
                //Application.Exit();
                Environment.Exit(0);
            }
        }

        private void settings_Click(object sender, EventArgs e)
        {
            Form2 newForm = new Form2();
            newForm.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            RegAdress = Properties.Settings.Default.GenAdress;
            webBrowser2.Navigate(RegAdress);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
