﻿namespace SimSim
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BildAdress = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.GenAdress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.firstpause = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dirPath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BildAdress
            // 
            this.BildAdress.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BildAdress.Location = new System.Drawing.Point(110, 37);
            this.BildAdress.Name = "BildAdress";
            this.BildAdress.Size = new System.Drawing.Size(186, 18);
            this.BildAdress.TabIndex = 12;
            this.BildAdress.TextChanged += new System.EventHandler(this.BildAdress_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Адрес билайна";
            // 
            // GenAdress
            // 
            this.GenAdress.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GenAdress.Location = new System.Drawing.Point(110, 13);
            this.GenAdress.Name = "GenAdress";
            this.GenAdress.Size = new System.Drawing.Size(186, 18);
            this.GenAdress.TabIndex = 10;
            this.GenAdress.TextChanged += new System.EventHandler(this.GenAdress_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Адрес генерации";
            // 
            // firstpause
            // 
            this.firstpause.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.firstpause.Location = new System.Drawing.Point(137, 68);
            this.firstpause.Name = "firstpause";
            this.firstpause.Size = new System.Drawing.Size(36, 18);
            this.firstpause.TabIndex = 14;
            this.firstpause.TextChanged += new System.EventHandler(this.firstpause_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Первая пацуза печати";
            // 
            // dirPath
            // 
            this.dirPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dirPath.Location = new System.Drawing.Point(120, 106);
            this.dirPath.Name = "dirPath";
            this.dirPath.Size = new System.Drawing.Size(145, 18);
            this.dirPath.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Папка для данных";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(271, 102);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 22);
            this.button1.TabIndex = 19;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.ClientSize = new System.Drawing.Size(307, 158);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dirPath);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.firstpause);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BildAdress);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.GenAdress);
            this.Controls.Add(this.label1);
            this.Name = "Form2";
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox BildAdress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox GenAdress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox firstpause;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dirPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
    }
}