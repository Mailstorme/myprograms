﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;
using mshtml;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using System.Net;

namespace SimSim
{
    public partial class Form1 : Form
    {
        string[][] arr;
        int N;
        bool test = false;
        bool pauseee = false;
        int testt;
        string mess;
        string thisDay = DateTime.Today.ToString("d");
        bool GoNextIfErr;



        int M;
        string[] form_inputs = new string[] {"customer.person.surname","customer.person.name", "customer.person.patronymic","dtBd",
            "customer.person.passportDocument.series","customer.person.passportDocument.number", "dtGb","mts_kodtochki","customer.person.passportDocument.givenBy","customer.person.citizenship",
            "customer.address.place.name","customer.address.street.name","customer.address.house", "customer.address.building.name","customer.address.room.name","customer.address.country",
            "bee_number","bee_simcard","bee_tarif","mts_data"};

        string[] aiva_name_inputs = new string[] {"p_1122155769","p2003666261", "inf1527395","p_1463878312",
            "customer.person.passportDocument.series","customer.person.passportDocument.number", "dtGb","mts_kodtochki","customer.person.passportDocument.givenBy","customer.person.citizenship",
            "a$5500420000000000000067","a$5500420000000000000027","a$5500420000000000000028", "customer.address.building.name","a$5500420000000000000032","customer.address.country",
            "bee_number","bee_simcard","bee_tarif","mts_data"};

        string[] Sim_name_inputs = new string[] {"p_1122155769","p2003666261", "p_1710113691","p_1463878312",
            "p1027860784","customer.person.passportDocument.number", "p1180261420","mts_kodtochki","p_65043984","customer.person.citizenship",
            "p_679377118","p_1810875006","p1620987020", "p_2044514753","p_716700333","customer.address.country",
            "bee_number","bee_simcard","bee_tarif","p3960917"};

        public Form1()
        {
            InitializeComponent();


            int RegVal = 1101;
            using (RegistryKey Key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", RegistryKeyPermissionCheck.ReadWriteSubTree))
                if (Key.GetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe") == null)
                    Key.SetValue(System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe", RegVal, RegistryValueKind.DWord);  // для запуска 11-го ядра webbrowser

            webBrowser1.Navigate("");

            webBrowser2.Navigate("http://10.8.0.1/printing/index.php");
            workStatus.BackColor = Color.FromArgb(100, 255, 0, 0);

        }


        public void arrcreate()
        {
            N = 0;
            M = 0;

            foreach (HtmlElement he in webBrowser2.Document.GetElementsByTagName("div"))
            {
                if (he.GetAttribute("className").Contains("fon_beeline"))
                {
                    N++;
                }
            }

            arr = new string[N + 1][];
            arr[0] = new string[] {"bee_family","bee_name", "bee_otchestvo","bee_date_rogden",
            "bee_pasport_seria","bee_pasport_nomer","bee_pasport_vidan_date","mts_kodtochki","bee_pasport_vidan","vidan_country",
            "bee_city_gorod","bee_city_street","bee_city_dom", "bee_city_korpus","bee_city_kvartira","adress_contry",
            "bee_number","bee_simcard","bee_tarif","mts_data","pol","CountryNumber" };  // Атрибуты для вставки в форму
            M = arr[0].Length;
            int k = 0;

            foreach (HtmlElement he in webBrowser2.Document.GetElementsByTagName("div")) // Создание Массива 
            {
                if (he.GetAttribute("className").Contains("fon_beeline"))
                {
                    k++;
                    arr[k] = new string[M];
                }
                for (int i = 0; i < M; i++)
                {
                    if (he.GetAttribute("className") == (arr[0][i]))
                    {
                        arr[k][i] = he.InnerText;
                        break;
                    }
                    if (he.GetAttribute("className") == "bee_pol_m")
                    {
                        if(he.OuterHtml.Contains("img"))
                            arr[k][Array.IndexOf(arr[0], "pol")] = "m";
                        else
                            arr[k][Array.IndexOf(arr[0], "pol")] = "g";
                        break;
                    }
                }
            }

            for (int i = 1; i < N+1; i++)
            {

                #region Номер страны
                switch (arr[i][8])
                {
                    case "008 р. Армения":
                        arr[i][9] = "Армения";
                        arr[i][Array.IndexOf(arr[0], "CountryNumber")] = "051";
                        break;
                    case "МИД Азербайджан":
                        arr[i][9] = "Азербайджан";
                        arr[i][Array.IndexOf(arr[0], "CountryNumber")] = "031";
                        break;
                    case "МИД Узбекистана":
                        arr[i][9] = "Узбекистан";
                        arr[i][Array.IndexOf(arr[0], "CountryNumber")] = "860";
                        break;
                    case "ПВО Киргизии":
                        arr[i][9] = "Киргизия";
                        arr[i][Array.IndexOf(arr[0], "CountryNumber")] = "417";
                        break;
                    case "МВД Р. Беларусь":
                        arr[i][9] = "Беларусь";
                        arr[i][Array.IndexOf(arr[0], "CountryNumber")] = "112";
                        break;
                    case "ОО Кишинев":
                        arr[i][9] = "Молдова";
                        arr[i][Array.IndexOf(arr[0], "CountryNumber")] = "498";
                        break;
                    case "УМВД Украины":
                        arr[i][9] = "Украина";
                        arr[i][Array.IndexOf(arr[0], "CountryNumber")] = "804";
                        break;
                    case "МИД Р. Грузия":
                        arr[i][9] = "Грузия";
                        arr[i][Array.IndexOf(arr[0], "CountryNumber")] = "268";
                        break;
                    default:
                        MessageBox.Show("Ошибка сопоставления страны и выдачи паспорта");
                        break;
                }
                #endregion


                if ((arr[i][2] == "undefined") || (arr[i][2] == null))
                    arr[i][2] = ""; 


                if ((arr[i][Array.IndexOf(arr[0], "bee_city_korpus")] == "") || (arr[i][Array.IndexOf(arr[0], "bee_city_korpus")] == "undefined") || (arr[i][Array.IndexOf(arr[0], "bee_city_korpus")] == null))
                    arr[i][Array.IndexOf(arr[0], "bee_city_korpus")] = "";


                arr[i][Array.IndexOf(arr[0], "bee_pasport_seria")] += " " + arr[i][Array.IndexOf(arr[0], "bee_pasport_nomer")];
                arr[i][Array.IndexOf(arr[0], "adress_contry")] = "Россия";
                arr[i][Array.IndexOf(arr[0], "bee_pasport_vidan_date")] = DateTrandform(arr[i][Array.IndexOf(arr[0], "bee_pasport_vidan_date")]);
                arr[i][Array.IndexOf(arr[0], "bee_date_rogden")] = DateTrandform(arr[i][Array.IndexOf(arr[0], "bee_date_rogden")]);
                arr[i][Array.IndexOf(arr[0], "mts_data")] = DateTrandform(arr[i][Array.IndexOf(arr[0], "mts_data")]);

            }
                
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        #region BEELINE          

        public bool waitingButtonElelement(int time, string attribute, string element, string Outer)
        { 
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (true)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < time * 1000)
                {
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button"))
                    {
                        if (he.GetAttribute(attribute) == element && he.OuterHtml.Contains(Outer))
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    return false;
                }
                pause(100);
            }
        }



        public bool waitingButtonElelement(int time, string attribute, string element, string Outer, string attribute2, string element2, string Outer2)
        {
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (true)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < time * 1000)
                {
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button"))
                    {
                        if (he.GetAttribute(attribute) == element && he.OuterHtml.Contains(Outer))
                        {
                            return true;
                        }
                        if (he.GetAttribute(attribute2) == element2 && he.OuterHtml.Contains(Outer2))
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    return false;
                }
                pause(100);
            }
        }



        public bool waitingInputElelement(int time, string atr1, string el1, string atr2, string el2)
        {
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (true)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < time * 1000)
                {
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
                    {
                        if (he.GetAttribute(atr1) == el1 && he.GetAttribute(atr2) == el2)
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            return false;
        }



        public bool waitingElelementWithOuterHtml(int time, string tag, string atr1, string el1, string Outer)
        {
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (true)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < time * 1000)
                {
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName(tag))
                    {
                        if (he.GetAttribute(atr1) == el1 && he.OuterHtml.Contains(Outer))
                        {
                            return true;
                        }
                    }
                }
                else
                    return false;
                pause(100);
            }
        }



        public bool waitingSelectElelement(int time, string atr1, string el1)
        {
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (true)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < time * 1000)
                {
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("Select"))
                    {
                        if (he.GetAttribute(atr1) == el1)
                        {
                            if (he.Children.Count >= 2)
                                return true;
                        }
                    }
                }
                else
                    return false;

                pause(100);
            }
        }



        public bool waitingIdElelement(int time, string id)
        {
            bool finde = false;
            bool timeEnd = false;
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (!finde)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < time * 1000)
                {
                    finde = true;
                    try
                    {
                        webBrowser1.Document.GetElementById(id);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        pause(100);
                        finde = false;
                    }
                }
                else
                {
                    timeEnd = true;
                }

                if (finde || timeEnd)
                {
                    break;
                }
                pause(50);
            }

            if (finde)
                return true;
            else
                return false;
        }



        public bool waitingInputSelect(int time, string atr1, string el1, string atr2, string el2)
        {
            bool finde = false;
            bool timeEnd = false;
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (!finde)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < time * 1000)
                {
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
                    {
                        if (he.GetAttribute(atr1) == el1 && he.GetAttribute(atr2) == el2)
                        {
                            finde = true;
                            break;
                        }
                    }
                }
                else
                {
                    timeEnd = true;
                }
                if (finde || timeEnd)
                {
                    break;
                }
                pause(100);
            }

            if (finde)
                return true;
            else
                return false;
        }



        public bool buttonClick(string tag, string attribute, string element, string Outer)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button"))
            {
                if (he.GetAttribute(attribute) == element && he.OuterHtml.Contains(Outer))
                {
                    he.InvokeMember("click");
                    return true;
                }
            }
            return false;
        }


        
        public bool ErrCheck()
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("p"))
            {
                if (he.OuterHtml.Contains("уже существует и находится в статусе, не допускающем редактирование"))
                {
                    return true;
                }
            }
            return false;
        }



        public bool BottomFormCheck(int time)
        {

            bool finde = false;
            bool timeEnd = false;
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (!finde && !timeEnd)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < time * 1000)
                {
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("div"))
                    {
                        if (he.GetAttribute("show-panel") == "newcontract.editAbonentInfo")
                            if (he.GetAttribute("className") == "ng-isolate-scope")
                                finde = true;
                    }
                }
                else
                {
                    timeEnd = true;
                }
            }
            return finde;
        }



        public void fillInput(string element, string str)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
            {
                if (he.GetAttribute("type") == element)
                {
                    he.Focus();
                    SendKeys.SendWait(str);
                }
            }
        }



        public void fillInput(string attribute, string element, string str)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
            {
                if (he.GetAttribute(attribute) == element)
                {
                    he.Focus();
                    SendKeys.SendWait(str);
                    break;
                }
            }
        }



        public bool fillAllForm(string tag, string attribute, int i)
        {
            MessageBox.Show("2");
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName(tag))
            {
                int k = Array.IndexOf(form_inputs, he.GetAttribute(attribute));
                
                if (k != -1)
                {
                    if (he.GetAttribute(attribute) == "customer.address.room.name")
                        return true;
                    he.Focus();
                    SendKeys.SendWait(arr[i][k]);
                    pause(100);
                }
            }
            return true;
        }



        public bool fillActivDate(int i)
        {

            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            bool Load = false;
            while (!Load)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < 8 * 1000)
                {
                    pause(100);
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
                    {
                        if (he.GetAttribute("Value") == "ЧАСТНОЕ ЛИЦО")
                        {
                            Load = true;
                            pause(50);
                            break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Пункт \"Частное Лицо\" не загружен");
                    return false;
                }
            }




            bool finde = false;
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
            {
                if (he.GetAttribute("data-ng-model") == "newcontract.dtContractDate")
                {
                    finde = true;
                }
            }
            if (!finde)
            {
                return false;
            }

            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
            {
                if (he.GetAttribute("Value") == "ЧАСТНОЕ ЛИЦО")
                {
                    he.Focus();
                    pause(100);
                    SendKeys.SendWait("{TAB}");
                    pause(100);
                    SendKeys.SendWait(DateTransf(arr[i][19]));
                    return true;
                }
            }
            return false;
        }



        public bool fillSelectPasport(string id)
        {
            HtmlElement g = webBrowser1.Document.GetElementById(id);

            g.Focus();
            pause(50);
            SendKeys.SendWait("{DOWN}");
            SendKeys.SendWait("{DOWN}");
            return true;
        }


        
        public bool fillSelectPol(int num, string pol)
        {

            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("select"))
            {
                if (he.GetAttribute("data-ng-model") == "customer.person.sex")
                {
                    he.Focus();
                    pause(50);
                    if (pol == "g")
                        SendKeys.SendWait("{DOWN}");
                    SendKeys.SendWait("{DOWN}");
                    return true;
                }
            }
            return true;
        }



        public bool fillSelectGorod(string id, int num)  // personPlaceType
        {
            HtmlElement g = webBrowser1.Document.GetElementById(id);
            g.Focus();
            pause(50);
            SendKeys.SendWait("{DOWN}");
            return true;
        }



        public bool fillSelectStreet(string id, int num)  // personStreetType
        {
            HtmlElement g = webBrowser1.Document.GetElementById(id);

            g.Focus();
            pause(50);
            SendKeys.SendWait("{DOWN}");
            return true;
        }



        public bool LoadCircleWait(int t)                    // Ожидание загрузочного кружка.  t - время в секундах
        {
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            bool Load = false;
            while (true)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < t * 1000)
                {
                    Load = true;
                    foreach (HtmlElement hh in webBrowser1.Document.GetElementsByTagName("DIV"))
                    {
                        if (hh.GetAttribute("className").Contains("modal-bg ng-scope active"))
                        {
                            pause(100);
                            Load = false;
                        }
                    }
                    if (!Load)
                        continue;
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
        }



        public bool LoadCircl()
        {
            foreach (HtmlElement hh in webBrowser1.Document.GetElementsByTagName("DIV"))
            {
                if (hh.GetAttribute("className").Contains("modal-bg ng-scope active"))
                {
                    return true;
                }
            }
            return false;
        }



        private void pause(int sleep)
        {
            double b = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while ((DateTime.Now.TimeOfDay.TotalMilliseconds - b) < sleep)
                Application.DoEvents();
        }



        private void ResidentCheck()
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("div"))
            {
                if (he.GetAttribute("className") == "form-item doc ng-hide")
                    webBrowser1.Document.GetElementById("isResidentCheck").InvokeMember("click");
            }
        }



        public string DateTransf(string date)
        {
            string str = "";

            if (date.Length == 10)
                str = date.Substring(6, 4) + "-" + date.Substring(3, 2) + "-" + date.Substring(0,2);
            else
                if (date.Length == 8)
                    str = date.Substring(4, 4) + "-" + date.Substring(2, 2) + "-" + date.Substring(0, 2);

            return str;
        }

                
        public bool WaitingNewContractPage()
        {
            bool enter = false;
            for (int il = 0; il < 6; il++)
            {
                if (waitingInputElelement(1, "type", "text", "data-ng-model", "transfer.iccid") &&
                    waitingInputElelement(1, "type", "text", "data-ng-model", "transfer.checkCode"))
                {
                    enter = true;
                    break;
                }
            }
            if (!enter)
            {
                MessageBox.Show("Перейдите на страницу нового договора");
                while (true)
                {
                    if (waitingInputElelement(1, "type", "text", "data-ng-model", "transfer.iccid") &&
                    waitingInputElelement(1, "type", "text", "data-ng-model", "transfer.checkCode"))
                    {
                        break;
                    }
                }
                pause(300);
            }

            return true;
        }


        public bool IccidAndNumber(int i)
        {
            foreach(HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
            {
                if (he.GetAttribute("type") == "text")
                {
                    if (he.GetAttribute("value") != "")
                        he.SetAttribute("value", "");
                }
            }
            fillInput("data-ng-model", "transfer.iccid", arr[i][17]);
            fillInput("data-ng-model", "transfer.checkCode", arr[i][16].Substring(6, arr[i][16].Length - 6));
            return true;
        }


        public bool InputButtonClick(string outer)
        {
            bool res =  false;
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
            {
                if (he.OuterHtml.Contains(outer))
                {
                    res = true;
                    he.InvokeMember("focus");
                    he.InvokeMember("click");
                }
            }
            return res;
        }



        /////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////

        private void button1_Click(object sender, EventArgs e)    // НАЖАЛИ СТАРТ
        {
            testt = 0;
            bool enter1 = false;
            foreach (HtmlElement he in webBrowser2.Document.GetElementsByTagName("div"))  // Проверка на Генерацию
            {
                if (he.GetAttribute("className").Contains("fon_beeline"))
                {
                    enter1 = true;
                }
            }
            if (!enter1)
            {
                MessageBox.Show("Сгенерируйте список");
                return;
            }

            workStatus.Text = "работает";
            workStatus.BackColor = System.Drawing.Color.Lime;

            #region для тестов 
            //arr = new string[2][];
            //arr[0] = new string[] {"bee_family","bee_name", "bee_otchestvo","bee_date_rogden",
            //"bee_pasport_seria","bee_pasport_nomer","bee_pasport_vidan_date","kod_podr","bee_pasport_vidan","vidan_country",
            //"bee_city_gorod","bee_city_street","bee_city_dom", "bee_city_korpus","bee_city_kvartira","agentSigningName",
            //"bee_number","bee_simcard","bee_tarif", "mts_data","CountryNumber" };
            //arr[1] = new string[] {"Назибов","Назиб", "Назибович","03.07.1992",
            //"qw","12321312","01.09.2000","-","AAA","AAA",
            //"Москва","Лучшая","15", "Б","533","agentSigningName",
            //"78888888888","88888888888888888888888","bee_tarif","03.11.2017","051" };
            //N = 1;
            #endregion


            arrcreate();

            for (int i = 1; i < N + 1; i++)
            {
                WaitingNewContractPage();

                label1.Text = i.ToString() + " из " + N.ToString();

                IccidAndNumber(i);

                pause(100);

                if (!InputButtonClick("Перейти к договору"))
                {
                    MessageBox.Show("Кнопка \"Перейти к договору\" не найдена");
                    webBrowser1.Navigate(" ");
                    continue;
                }

                if (!LoadCircleWait(20))
                {
                    MessageBox.Show("Кружок крутится, как закончит, нажмите кнопку \"OK\"");
                }

                if (ErrCheck())
                {
                    webBrowser1.Navigate(" ");
                    continue;
                }


                bool butt = true; 
                if (!waitingButtonElelement(6, "data-ng-click", "newcontract.showAbonentInfo()", "Заполнить", "data-ng-click", "preview.goToEdit()", "Редактировать"))
                {
                    butt = false;
                    if (!waitingButtonElelement(6, "data-ng-click", "preview.goToEdit()", "Редактировать"))
                    {
                        MessageBox.Show("Кнопки \"Заполнить\" и \"Редактировать\" не найдены");
                        return;
                    }
                }


                if (butt)
                {
                    bool fillDate = fillActivDate(i);
                    if (!fillDate)
                    {
                        MessageBox.Show("Не нашли кнопку даты активации номера, либо проблема с полем \"Частное Лицо\"");
                        webBrowser1.Navigate(" ");
                        continue;
                    }
                }



                if (butt)
                    if (!buttonClick("button", "data-ng-click", "newcontract.showAbonentInfo()", "Заполнить"))
                    {
                        MessageBox.Show("Кнопка ЗАПОЛНИТЬ, для открытия нижней формы не найдена");
                        webBrowser1.Navigate(" ");
                        continue;
                    }
                    else
                    {
                        buttonClick("button", "data-ng-click", "preview.goToEdit()", "Редактировать");

                        if (!waitingButtonElelement(6, "data-ng-click", "newcontract.showAbonentInfo()", "Заполнить"))
                        {
                            MessageBox.Show("Кнопка \"Заполнить\" не найдена");
                        }
                        buttonClick("button", "data-ng-click", "newcontract.showAbonentInfo()", "Заполнить");
                    }
                

                if (!LoadCircl())
                {
                    label3.Text = "НЕТ КРУЖКА" + testt.ToString();
                    pause(200);
                    if (butt)
                    {
                        if (!buttonClick("button", "data-ng-click", "newcontract.showAbonentInfo()", "Заполнить"))
                        {
                            MessageBox.Show("Кнопка ЗАПОЛНИТЬ, для открытия нижней формы не найдена");
                            webBrowser1.Navigate(" ");
                            continue;
                        }
                    }
                    else
                    {
                        buttonClick("button", "data-ng-click", "preview.goToEdit()", "Редактировать");

                        if (!waitingButtonElelement(6, "data-ng-click", "newcontract.showAbonentInfo()", "Заполнить"))
                        {
                            MessageBox.Show("Кнопка \"Заполнить\" не найдена");
                        }
                        buttonClick("button", "data-ng-click", "newcontract.showAbonentInfo()", "Заполнить");
                    }
                }


                if (!LoadCircleWait(20))
                {
                    MessageBox.Show("Кружок крутится, как закончит, нажмите кнопку \"OK\"");
                }


                if (!waitingInputElelement(12, "type", "text", "data-ng-blur", "copySurnameToContact()") || !waitingInputElelement(12, "type", "text", "data-ng-model", "customer.address.room.name")
                    || !waitingSelectElelement(12, "id", "personDocType"))
                {
                    MessageBox.Show("Ошибка загрузки формы");
                    return;
                }
                
                
                if (!BottomFormCheck(4))
                {
                    MessageBox.Show("Ошибка обращения к нижней форме");
                    webBrowser1.Navigate(" ");
                    continue;
                }
                if (!LoadCircleWait(20))
                {
                    MessageBox.Show("Кружок крутится, как закончит, нажмите кнопку \"OK\"");
                }


                ResidentCheck();


                fillSelectPasport("personDocType");
                fillSelectPol(1,arr[i][Array.IndexOf(arr[0], "pol")]);
                fillSelectGorod("personPlaceType", 1);
                fillSelectStreet("personStreetType", 1);
                if (arr[i][Array.IndexOf(arr[0], "bee_city_korpus")] != null && arr[i][Array.IndexOf(arr[0], "bee_city_korpus")] != "")
                    fillSelectStreet("personBuildingType", 1);
                if (arr[i][Array.IndexOf(arr[0], "bee_city_kvartira")] != null && arr[i][Array.IndexOf(arr[0], "bee_city_kvartira")] != "")
                    fillSelectStreet("personRoomType", 1);

                
                foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))
                {
                    if (he.GetAttribute("data-ng-model").Contains("customer.address.country"))
                        if (he.GetAttribute("value") != "")
                            he.SetAttribute("value", "");
                }

                fillAllForm("input", "data-ng-model", i);    
                fillAllForm("textarea", "data-ng-model", i);

                fillInput("data-ng-model", "customer.address.room.name",arr[i][14].Replace("кв.","").Replace("ком.", "").Replace("КВ.", "").Replace("КОМ.", ""));

                pause(1000);

                if (!InputButtonClick("Отправить на регистрацию"))
                {
                    MessageBox.Show("Проверьте форму и сохраните контракт");
                }

                LoadCircleWait(4);
                pause(1000);
                buttonClick("button", "data-ng-click", "preview.printAgreement()", "Распечатать");
               


                //string statuswaitforpdf;
                //SHDocVw.ShellWindows shellWindows = new SHDocVw.ShellWindows();

                //statuswaitforpdf = "";
                //while (!statuswaitforpdf.Contains("report_window.html"))
                //{
                //    Application.DoEvents();
                //    foreach (SHDocVw.IWebBrowser2 ie in shellWindows)
                //    {
                //        try
                //        {
                //            statuswaitforpdf = ie.LocationURL;
                //        }
                //        catch { }
                //    }
                //}

                webBrowser1.Navigate(" ");
            }


            workStatus.Text = "Конец";
            workStatus.BackColor = System.Drawing.Color.Violet;

        }

        #endregion


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        #region AIVA

        public bool FillAllForm(string tag, string attribute, int i)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName(tag))
            {
                int k = Array.IndexOf(Sim_name_inputs, he.GetAttribute(attribute));

                if (k != -1)
                {
                    //if (he.GetAttribute(attribute) == "customer.address.room.name")
                    //    return true;
                    
                    he.SetAttribute("value",arr[i][k]);
                    he.InvokeMember("onblur");

                    he.InvokeMember("onkeyup");
                    //SendKeys.SendWait(arr[i][k]);
                }
            }
            return true;
        }


        public HtmlElement ReturnHtmlElement(string tag, string attribute, string element)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName(tag))
            {
                if (he.GetAttribute(attribute) == element)
                {
                    return he;
                }
            }
            return null;
        }



        public HtmlElement ReturnHtmlElement(string tag, string attribute, string element, string Outer)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName(tag))
            {
                if (he.GetAttribute(attribute) == element && he.OuterHtml.Contains(Outer))
                {
                    return he;
                }
            }
            return null;
        }



        public bool JSfillNumber(string name, string number)
        {
            HtmlElement head = webBrowser1.Document.GetElementsByTagName("head")[0];
            HtmlElement scriptEl = webBrowser1.Document.CreateElement("script");
            IHTMLScriptElement element = (IHTMLScriptElement)scriptEl.DomElement;
            element.text = "function FillNumber() { document.getElementsByName('" + name + "')[0].value='" + number + "' }";
            head.AppendChild(scriptEl);
            webBrowser1.Document.InvokeScript("FillNumber");

            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("input"))  // Заставляем появиться кнопку "октыть"
            {
                if (he.OuterHtml.Contains("a$5500420000000000000472"))
                {
                    //he.InvokeMember("data-on-enter");
                    he.InvokeMember("focus");
                    continue;
                }
                if (he.OuterHtml.Contains("a$5500420000000000000473"))
                {
                    //he.InvokeMember("data-on-enter");
                    he.InvokeMember("focus");
                    continue;
                }
            }

            return true;
        }



        public void JSfillDate(string name, string number)
        {
            HtmlElement head = webBrowser1.Document.GetElementsByTagName("head")[0];
            HtmlElement scriptEl = webBrowser1.Document.CreateElement("script");
            IHTMLScriptElement element = (IHTMLScriptElement)scriptEl.DomElement;
            element.text = "function FillNumberr() { document.getElementsByClassName('" + name + "')[0].value='" + number + "' }";
            head.AppendChild(scriptEl);
            webBrowser1.Document.InvokeScript("FillNumberr");
        }



        public void FillElement(string tag, string attribute , string element, string str)
        {
            foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName(tag))
            {
                if (he.GetAttribute(attribute) == element)
                {
                    he.SetAttribute("value",str);
                    MessageBox.Show("вставили");
                }
            }
        }



        public void SaveWebbrowserInTxt(WebBrowser wb, string path)
        {
            Directory.CreateDirectory(path);

            System.IO.StreamWriter textFile;
            textFile = new System.IO.StreamWriter(path, true);

            string wb2list = wb.DocumentText;
            Encoding encoding = Encoding.GetEncoding(1251);
            Stream stream = wb.DocumentStream;
            StreamReader sr = new StreamReader(stream, encoding);
            wb2list = sr.ReadToEnd();
            stream.Close();
            textFile.Write(wb2list);                              // Сохраняем Сгенерированный список
            textFile.Close();
        }



        public delegate void methodContainer(int i, string str, string path);
        public event methodContainer CorrectNum;
        

        public void Correct(int i, string str, string path)
        {
            string contract = i.ToString() + ") " + arr[i][Array.IndexOf(arr[0], "bee_number")] + "  +" + str;

            richTextBox1.AppendText(contract + "\n");
        }



        public void InCorrect(int i, string str)
        {
             string contract = i.ToString() + ") " + arr[i][Array.IndexOf(arr[0], "bee_number")] + "  - " + str;

            richTextBox1.AppendText(contract + "\n");
        }



        public void ErPause()
        {
            GoNextIfErr = false;

            workStatus.Text = "пауза";
            workStatus.BackColor = Color.FromArgb(255, 255, 255, 0);

            while (!GoNextIfErr)
                pause(300);

            workStatus.Text = "работает";
            workStatus.BackColor = System.Drawing.Color.Lime;
        } 


        public void SaveTxtContract(int i, string str, string path)
        {
            Directory.CreateDirectory(path);
            path = path + "\\" + thisDay + ".txt";
            string contract = "";
            string Date = DateTime.Now.ToString("dd MMMM yyyy | HH:mm:ss");

            contract = "\n" + i.ToString() + ") < " + arr[i][Array.IndexOf(arr[0], "mts_nomer")] + " >  -- " + Date + "\n";
            for (int j = 0; j < M; j++)
            {
                contract += "    -" + arr[i][j] + "-" + "\n";
            }
            StreamWriter sw;
            if (!File.Exists(path))
            {
                sw = new StreamWriter(path, true);
                sw.WriteLine("Отправленные номера за " + thisDay + "\n" + "\n");
                sw.Close();
            }

            //string pathsw = @"C:\MegafonProga\" + thisDay + "\\" + "Отправленные номера.txt";
            sw = new StreamWriter(path, true);
            sw.WriteLine(contract);
            sw.Close();
        }


        public void SaveTxt(string str, string path)
        {
            Directory.CreateDirectory(path);
            path = path + "\\" + thisDay + ".txt";
            string contract = str;
            
            
            StreamWriter sw;
            if (!File.Exists(path))
            {
                sw = new StreamWriter(path, true);
                sw.WriteLine("OUTER " + thisDay + "\n" + "\n");
                sw.Close();
            }

            //string pathsw = @"C:\MegafonProga\" + thisDay + "\\" + "Отправленные номера.txt";
            sw = new StreamWriter(path, true);
            sw.WriteLine(contract + "\n" + "\n" + "\n" + "\n" + "--------____________________-------------------" + "\n" + "\n");
            sw.Close();
        }


        public bool Pasport(int i)
        {
            ReturnHtmlElement("div", "data-column-name", "a$100420000000000000106").Children[4].InvokeMember("click");

            waitingElelementWithOuterHtml(10, "input", "name", "a$100420000000000000049", "a$100420000000000000049");

            string numser = arr[i][Array.IndexOf(arr[0], "bee_pasport_seria")] + " " + arr[i][Array.IndexOf(arr[0], "bee_pasport_nomer")];
            ReturnHtmlElement("input", "name", "a$100420000000000000049").SetAttribute("value", numser);

            string date = arr[i][Array.IndexOf(arr[0], "bee_pasport_vidan_date")].Replace(" ",".");
            ReturnHtmlElement("span", "className", "control c-datetimepicker inited align-self_stretch", "is-masked=\"1\"").Children[0].SetAttribute("value", date);
            ReturnHtmlElement("span", "className", "control c-datetimepicker inited align-self_stretch", "is-masked=\"1\"").Children[0].InvokeMember("focus");
            

            string vidan = arr[i][Array.IndexOf(arr[0], "bee_pasport_vidan")];
            ReturnHtmlElement("input", "name", "a$100420000000000000047_int").SetAttribute("value", vidan);

            //string country = arr[i][Array.IndexOf(arr[0], "vidan_country")];
            //ReturnHtmlElement("span", "data-column-name", "a$5500420000000000000103").Children[0].Children[0].SetAttribute("value", country);

            ReturnHtmlElement("span", "data-column-name", "a$5500420000000000000103").Children[0].Children[1].InvokeMember("click");
            string countrynumber = arr[i][Array.IndexOf(arr[0], "CountryNumber")];
            waitingElelementWithOuterHtml(10, "div", "data-value", countrynumber, "listbox-item");
            ReturnHtmlElement("div", "data-value", countrynumber).InvokeMember("click");

            //pause(100);
            //ReturnHtmlElement("span", "className", "control c-datetimepicker inited align-self_stretch", "is-masked=\"1\"").Children[0].InvokeMember("focus");
            //pause(100);

            ReturnHtmlElement("input", "name", "a$100420000000000000049").InvokeMember("focus");
            pause(100);
            ReturnHtmlElement("button", "value", "Добавить").InvokeMember("click");

            return true;
        }


        public int WaitingOpenContractButton(int i)
        {
            for (int k = 0; k < 5 ; k ++)
            {
                if (waitingElelementWithOuterHtml(1, "a", "target", "_blank", "Открыть..."))
                    return 1;
                if (webBrowser1.Document.GetElementById("msisdn_mtt") != null)
                    if (webBrowser1.Document.GetElementById("msisdn_mtt").InnerHtml != null)
                        if (webBrowser1.Document.GetElementById("msisdn_mtt").InnerHtml.Contains("SIM-карта"))
                        {
                            InCorrect(i, webBrowser1.Document.GetElementById("msisdn_mtt").InnerHtml);
                            return 0;
                        }
            }
            InCorrect(i, "неизвестная ошибка загрузки кнопки" + webBrowser1.Document.GetElementById("msisdn_mtt").InnerHtml);
            return 0;
        }


        public bool ButtonWaiting(int time)
        {
            bool finde = false;
            bool timeEnd = false;
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (!finde)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < time * 1000)
                {
                    foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button"))
                    {
                        if (he.GetAttribute("title") == "Установить дату заключения договора" && he.GetAttribute("type") == "submit")
                        {
                            finde = true;
                            he.InvokeMember("click");
                            break;
                        }
                    }
                }
                else
                {
                    timeEnd = true;
                }
                if (finde || timeEnd)
                {
                    break;
                }
                pause(100);
            }

            return finde;
        }


        public bool TwoPauseWaiting()
        {
            GoNextIfErr = false;
            pauseee = false;

            workStatus.Text = "пауза";
            workStatus.BackColor = Color.FromArgb(255, 255, 255, 0);

            while (!GoNextIfErr && !pauseee)
            {
                pause(300);
            }

            workStatus.Text = "работает";
            workStatus.BackColor = System.Drawing.Color.Lime;

            return !GoNextIfErr;

        }


        public bool JSclickinput(string name)
        {
            HtmlElement head = webBrowser1.Document.GetElementsByTagName("head")[0];
            HtmlElement scriptEl = webBrowser1.Document.CreateElement("script");
            IHTMLScriptElement element = (IHTMLScriptElement)scriptEl.DomElement;
            element.text = "function FillNumbe() { document.getElementsByName('" + name + "')[0].onfocus }";
            head.AppendChild(scriptEl);
            webBrowser1.Document.InvokeScript("FillNumbe");


            return true;
        }


        public bool CloseContractWait()
        {
            bool b1 = false;
            bool b2 = true;
            while (!b1 || b2)
            {
                pause(100);
                b1 = false;
                b2 = false;

                foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("button"))
                {
                    if (he.GetAttribute("title") == "Сохранить" && he.GetAttribute("type") == "submit")
                    {
                        b1 = true;
                    }
                }

                foreach (HtmlElement he in webBrowser1.Document.GetElementsByTagName("div"))
                {
                    if (he.GetAttribute("className") == "add-form")
                    {
                        b2 = true;
                    }
                }
                pause(50);
            }

            return true;
        }


        private void button2_Click(object sender, EventArgs e)
        {

            #region для тестов 
            //arr = new string[2][];
            //arr[0] = new string[] {"bee_family","bee_name", "bee_otchestvo","bee_date_rogden",
            //"bee_pasport_seria","bee_pasport_nomer","bee_pasport_vidan_date","kod_podr","bee_pasport_vidan","vidan_country",
            //"bee_city_gorod","bee_city_street","bee_city_dom", "bee_city_korpus","bee_city_kvartira","agentSigningName",
            //"bee_number","bee_simcard","bee_tarif", "mts_data","CountryNumber" };
            //arr[1] = new string[] {"Назибов","Назиб", "Назибович","03.07.1992",
            //"qw","12321312","01.09.2000","-","AAA","AAA",
            //"Москва","Лучшая","15", "Б","533","agentSigningName",
            //"78888888888","88888888888888888888888","bee_tarif","03.11.2017","051" };
            //N = 1;
            #endregion

            CorrectNum += Correct;
            CorrectNum += SaveTxtContract;

            arrcreate();


            for (int i = 1; i < N + 1; i++)    // цикл
            {
                workStatus.Text = "работает";
                workStatus.BackColor = System.Drawing.Color.Lime;
                label1.Text = i.ToString() + " из " + N.ToString();

                while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                    Application.DoEvents();


                if (!waitingElelementWithOuterHtml(10, "input", "name", "a$5500420000000000000472", "a$5500420000000000000472"))
                {
                    MessageBox.Show("Перейдите на страницу активации нового контракта");
                    while(!waitingElelementWithOuterHtml(20, "input", "name", "a$5500420000000000000472", "a$5500420000000000000472"))
                        MessageBox.Show("Перейдите на страницу активации нового контракта");
                }

                JSfillNumber("a$5500420000000000000472", arr[i][Array.IndexOf(arr[0], "mts_nomer")]);

                if (WaitingOpenContractButton(i) == 0)
                {
                    webBrowser1.Navigate("");
                    continue;
                }


                webBrowser1.Navigate(webBrowser1.Document.GetElementById("msisdn_mtt").Children[0].GetAttribute("href"));

                waitingElelementWithOuterHtml(20, "input", "name", "a$100420000000000000002_int", "w-text ctrl-mli-input change_inited");

                FillAllForm("input","name",i);

                ReturnHtmlElement("span", "className", "control c-datetimepicker inited").Children[0].SetAttribute("value",arr[i][Array.IndexOf(arr[0], "bee_date_rogden")].Replace(" ","."));

                Pasport(i);

                pause(4000);

                try
                {
                    HtmlElement saveButt = null;
                    if ((saveButt = ReturnHtmlElement("button", "title", "Сохранить", "submit")) != null)
                        saveButt.InvokeMember("click");
                    else
                    {
                        InCorrect(i, "Не найдена кнопка \"Сохранить\" на форме");
                        webBrowser1.Navigate(" ");
                        continue;
                    }


                    if (!ButtonWaiting(15))
                    {
                        InCorrect(i, "кнопка '4' не найдена");
                        MessageBox.Show("Если форма заполнена неправильно, то сохраните верные данные и нажмите кнопку \"Установить дату заключения договора\", далее нажмите кнопку \"продолжить\" в программе. Если вы хотите начать запполнение с нового номера, то нажмите кнопку \"Следующий\" в программе");
                        if (!TwoPauseWaiting())
                        {
                            InCorrect(i, "кнопка \"Установить дату заключения договора\" (1) не найдена");
                            webBrowser1.Navigate(" ");
                            continue;
                        }
                    }

                    string dogovordate1 = arr[i][Array.IndexOf(arr[0], "mts_data")].Replace(" ", "");
                    dogovordate1 = dogovordate1.Substring(0, 4) + ".201" + dogovordate1.Substring(4, dogovordate1.Length - 4);
                    dogovordate1 = dogovordate1.Substring(0, 2) + "." + dogovordate1.Substring(2, dogovordate1.Length - 2);

                    //CorrectNum(i,"", Properties.Settings.Default.DIRPATH);
                    label3.Text = dogovordate1;

                    if (!waitingElelementWithOuterHtml(15, "div", "className", "add-form", "Дата заключения договора"))
                    {
                        MessageBox.Show("Не найдена форма даты заключения договора. Заполните дату заключения договора и сохраните контракт, затем нажмите кнопку \"Следующий\"");
                        ErPause();
                        webBrowser1.Navigate(" ");
                        continue;
                    }

                    pause(500); // 2000 было

                    foreach (HtmlElement he in ReturnHtmlElement("div", "className", "add-form").GetElementsByTagName("input"))
                    {
                        if (he.GetAttribute("className") == "w-text")
                        {
                            he.SetAttribute("value", dogovordate1.Replace(".",""));
                            he.InvokeMember("focus");
                            pause(50);
                        }
                    }

                    foreach (HtmlElement he in ReturnHtmlElement("div", "className", "add-form").GetElementsByTagName("input"))
                    {
                        if (he.GetAttribute("name") == "contract_date")
                        {
                            string date = dogovordate1.Substring(6, 4) + "-" + dogovordate1.Substring(3, 2) + "-" + dogovordate1.Substring(0, 2);
                            he.SetAttribute("value", date + " 00:00:00");
                        }
                    }

                    foreach (HtmlElement he in ReturnHtmlElement("div", "className", "add-form").GetElementsByTagName("button"))
                    {
                        if (he.GetAttribute("title") == "Установить дату заключения договора" && he.GetAttribute("type") == "submit")
                        {
                            he.InvokeMember("focus");
                            he.InvokeMember("click");
                        }
                    }
                }
                catch (Exception ec)
                {
                    MessageBox.Show("ВСЁ ПЛОХО!!!  -  " + ec.Message);
                }


                CloseContractWait();

                pause(100);
                webBrowser1.Navigate(" ");
            }


            workStatus.Text = "Конец";       workStatus.BackColor = System.Drawing.Color.Violet;
        }

        #endregion


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        #region SimSim

        private void browserChange_Click(object sender, EventArgs e)
        {
            if (webBrowser1.Visible == false)
            {
                browserChange.Text = "к генерации";
                webBrowser1.Visible = true;
                webBrowser2.Visible = false;
            }
            else
            {
                browserChange.Text = "на сайт";
                webBrowser1.Visible = false;
                webBrowser2.Visible = true;
            }
        }



        private void settings_Click(object sender, EventArgs e)
        {
            Form2 newForm = new Form2(this);
            newForm.Show();
        }



        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                Environment.Exit(0);
            }
        }



        private void entry_Click(object sender, EventArgs e)
        {

            if (comboBox2.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите агента");
                return;
            }

            string login = "";
            string pass = "";

            switch (comboBox2.Text)
            {
                case "М":
                    login = " ";
                    pass = " ";
                    break;
                case "И ":
                    login = " ";
                    pass = " ";
                    break;
                case "Бо":
                    login = " ";
                    pass = " ";
                    break;
                case "Сы":
                    login = " ";
                    pass = " ";
                    break;
                case "Ха":
                    login = "";
                    pass = " ";
                    break;
            }

            ReturnHtmlElement("input", "name", "p_323690313", "p_323690313").SetAttribute("value", login);
            ReturnHtmlElement("input", "name", "p_320401452", "p_320401452").SetAttribute("value", pass);
            ReturnHtmlElement("button", "apswidget", "ui.web_button", "Войти").InvokeMember("click");
        }



        private void testbutton_Click(object sender, EventArgs e)
        {
            pauseee = true;
        }


        private void button5_Click(object sender, EventArgs e)
        {
            GoNextIfErr = true;
        }



        //private static IWebDriver _webDriver;
        //private static string _mainWindowHandler;
        //private static IWebDriver WebDriver
        //{
        //    get { return _webDriver ?? StartWebDriver(); }
        //}

        private void button6_Click(object sender, EventArgs e)
        {
            ////var firefoxProfile = new FirefoxProfile
            ////{
            ////    AcceptUntrustedCertificates = true,
            ////    EnableNativeEvents = true
            ////};

            ////var internetExplorerOptions = new InternetExplorerOptions
            ////{
            ////    IntroduceInstabilityByIgnoringProtectedModeSettings = true,
            ////    InitialBrowserUrl = "about:blank",
            ////    EnableNativeEvents = true
            ////};

            ////_webDriver = new InternetExplorerDriver(Directory.GetCurrentDirectory(), internetExplorerOptions);


            ////pause(8000);

            ////string url = " ";
            ////WebDriver.Navigate().GoToUrl(url);


            //IWebDriver driver = new FirefoxDriver();
            //driver.Url = " ";


            //string URL = " ";
            //WebClient webClient = new WebClient();
            //webClient.DownloadFileAsync(new Uri(URL), @"C:\Users\Andrey\Desktop\file2.xls");
            //while (webClient.IsBusy)
            //{
            //    Application.DoEvents();
            //}
            //MessageBox.Show("!!!");



            //string URL = " ";
            ////webControl1.Source = new Uri(URL);

            //IWebDriver driver = new FirefoxDriver();
            //driver.Url = URL;


            //webBrowser1.Navigate(" ");
        }



        public bool ContractSave(string name,string path,string url)
        {
            string savepath = path + "\\" + name + ".xls";
            WebClient webClient = new WebClient();
            webClient.DownloadFileAsync(new Uri(url), savepath);


            workStatus.Text = "качаем";
            workStatus.BackColor = Color.FromArgb(255, 255, 255, 0);
            while (webClient.IsBusy)
                Application.DoEvents();
            workStatus.Text = "работает";
            workStatus.BackColor = System.Drawing.Color.Lime;

            if (File.Exists(savepath))
                return true;
            else
                return false;
        }


        public bool EnterPol(string pol)
        {

            int k = 0;
            if (pol == "g") k = 1;
            else if (pol == "m") k = 0;
            else MessageBox.Show("НЕВЕРНЫЙ ПОЛ");
            ReturnHtmlElement("select", "name", "p_2062842").Children[k].SetAttribute("selected", "selected");

            return true;
        }


        public string DateTrandform(string date)
        {
            string str = date;

            if (str.Length == 10)
                str = str.Replace(" ", ".");
            else
                if (str.Length == 8)
                str = date.Substring(0, 2) + "." + date.Substring(2, 2) + "." + date.Substring(4, 4);
            else
                MessageBox.Show("Неверно сгенерированный адрес");

            return str;
        }


        public int StrAfterId(string str)
        {
            if (!str.Contains("contract-id="))
                return 0;

            if (str.Length - str.IndexOf("contract-id=") - 12 > 0)
                return (str.Length - str.IndexOf("contract-id=") - 12);

            return 0;
        }


        public bool WaitingIdWebPage(int time)
        {
            double curtime = DateTime.Now.TimeOfDay.TotalMilliseconds;
            while (true)
            {
                if ((DateTime.Now.TimeOfDay.TotalMilliseconds - curtime) < time * 1000)
                {
                    string url = ReturnHtmlElement("a", "className", "btn btn-default pull-left", "Распечатать договор").GetAttribute("href");
                    if (StrAfterId(url) == 0)
                    {
                        pause(100);
                        continue;
                    }
                    else return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public void SaveString(string str, string path)
        {
            Directory.CreateDirectory(path);
            path = path + ".txt";
            StreamWriter sw;

            if (!File.Exists(path))
            {
                sw = new StreamWriter(path, true);
                sw.WriteLine("Дата создания файла: " + thisDay + "\n" + "\n");
                sw.Close();
            }

            sw = new StreamWriter(path, true);
            sw.WriteLine(str + "\n" + "\n");
            sw.Close();
        }


        private void button7_Click(object sender, EventArgs e)
        {
            #region для тестов 
            //arr = new string[2][];
            //arr[0] = new string[] {"bee_family","bee_name", "bee_otchestvo","bee_date_rogden",
            //"bee_pasport_seria","bee_pasport_nomer","bee_pasport_vidan_date","kod_podr","bee_pasport_vidan","vidan_country",
            //"bee_city_gorod","bee_city_street","bee_city_dom", "bee_city_korpus","bee_city_kvartira","agentSigningName",
            //"bee_number","bee_simcard","bee_tarif", "mts_data","CountryNumber" };
            //arr[1] = new string[] {"Назибов","Назиб", "Назибович","03.07.1992",
            //"qw","12321312","01.09.2000","-","AAA","AAA",
            //"Москва","Лучшая","15", "Б","533","agentSigningName",
            //"78888888888","88888888888888888888888","bee_tarif","03.11.2017","051" };
            //N = 1;
            #endregion


            bool enter1 = false;
            foreach (HtmlElement he in webBrowser2.Document.GetElementsByTagName("div"))  // Проверка на Генерацию
            {
                if (he.GetAttribute("className").Contains("fon_beeline"))
                {
                    enter1 = true;
                }
            }
            if (!enter1)
            {
                MessageBox.Show("Сгенерируйте список");
                return;
            }


            arrcreate();
            string dir = Properties.Settings.Default.DIRPATH + "//" + thisDay;
            Directory.CreateDirectory(dir);

            for (int i = 1; i < N + 1; i++)    // цикл
            {
                while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                    Application.DoEvents();

                workStatus.Text = "работает";
                workStatus.BackColor = System.Drawing.Color.Lime;
                label1.Text = i.ToString() + " из " + N.ToString();


                if (!waitingElelementWithOuterHtml(10, "input", "name", "p1979018837", "input form-control"))
                {
                    MessageBox.Show("Перейдите на страницу активации нового контракта");
                    while (!waitingElelementWithOuterHtml(20, "input", "name", "p1979018837", "input form-control"))
                        MessageBox.Show("Перейдите на страницу активации нового контракта");
                }


                string number = arr[i][Array.IndexOf(arr[0], "bee_number")];
                if (!number.Substring(0, 1).Contains("7")) number = "7" + number;
                ReturnHtmlElement("input", "name", "p1979018837", "text").SetAttribute("value", number);
                ReturnHtmlElement("span", "apswidget", "ui.text_button", "поиск").InvokeMember("click");

                pause(100);
                while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                    Application.DoEvents();

                if (!waitingElelementWithOuterHtml(10, "input", "name", "p_1122155769", "form-control"))
                {
                    InCorrect(i, "Страница формы не найдена");
                    webBrowser1.Navigate(" ");
                    continue;
                }


                FillAllForm("input","name",i);
                EnterPol(arr[i][Array.IndexOf(arr[0],"pol")]);

                if (!waitingElelementWithOuterHtml(4, "button", "className", "btn btn-primary", "Зарегистрировать"))
                {
                    InCorrect(i, "кнопка сохранения контракта не найдена");
                    webBrowser1.Navigate(" ");
                    continue;
                }

                //ErPause();  /////// del
                ReturnHtmlElement("button", "className", "btn btn-primary", "Зарегистрировать").InvokeMember("click");


                if (!waitingElelementWithOuterHtml(10, "a", "className", "btn btn-default pull-left", "Распечатать договор"))
                {
                    MessageBox.Show("Кнопка распечатки контракта не найдена, сохраните контракт и нажмите кнопку \"следующий номер\"");
                    ErPause();
                    webBrowser1.Navigate(" ");
                    continue;
                }

                if(!WaitingIdWebPage(6))
                {
                    MessageBox.Show("Не найдена страница загрузки контракта, сохраните контракт и нажмите кнопку \"следующий номер\"");
                    ErPause();
                    webBrowser1.Navigate(" ");
                    continue;
                }

                string url = ReturnHtmlElement("a", "className", "btn btn-default pull-left", "Распечатать договор").GetAttribute("href");

                try
                {
                    if (!ContractSave(i + ") " + number, dir, url))
                    {
                        MessageBox.Show("Сохраните контракт самостоятельно и нажмите кнопку \"следующий номер\"");
                        ErPause();
                        webBrowser1.Navigate(" ");
                        continue;
                    }
                }
                catch (Exception elw)
                {
                    SaveString(elw.Message, Properties.Settings.Default.DIRPATH + "\\БАГИ\\Ошибка");
                    MessageBox.Show(elw.Message);
                    MessageBox.Show("Ошибка сохранения контракта. Сохраните контракт самостоятельно и нажмите кнопку \"следующий номер\"");
                    ErPause();
                    webBrowser1.Navigate(" ");
                    continue;
                }

                Correct(i, "", "");

                pause(100);

                ReturnHtmlElement("button", "className", "btn btn-primary", "закрыть").InvokeMember("click");
                webBrowser1.Navigate(" ");

                pause(800);

            }

            workStatus.Text = "Конец";
            workStatus.BackColor = System.Drawing.Color.Red;
        }
    }
    #endregion
}
