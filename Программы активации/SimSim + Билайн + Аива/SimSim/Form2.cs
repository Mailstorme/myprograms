﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimSim
{
    public partial class Form2 : Form
    {
        private Form1 m_parent;
        bool Ch = false;

        public Form2()
        {
            InitializeComponent();
        }

        public Form2(Form1 frm1)
        {
            InitializeComponent();
            m_parent = frm1;
        }


        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Ch)
                return;
            bool ex = true;
            Properties.Settings.Default.GenAdress = GenAdress.Text;
            Properties.Settings.Default.BilAdress = BildAdress.Text;
            try
            {
                Properties.Settings.Default.firstpause = Convert.ToInt32(firstpause.Text);
            }
            catch
            {
                MessageBox.Show("неверный формат паузы");
                ex = false;
            }

            if (!ex)
            {
                e.Cancel = true;
                return;
            }

            Properties.Settings.Default.Save();
            MessageBox.Show("Внесенные изменения сохранены");
        }

        private void GenAdress_TextChanged(object sender, EventArgs e)
        {
            Ch = true;
        }

        private void BildAdress_TextChanged(object sender, EventArgs e)
        {
            Ch = true;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            GenAdress.Text = Properties.Settings.Default.GenAdress;
            BildAdress.Text = Properties.Settings.Default.BilAdress;
            firstpause.Text = Properties.Settings.Default.firstpause.ToString();
            dirPath.Text = Properties.Settings.Default.DIRPATH;
            Ch = false;
        }

        private void firstpause_TextChanged(object sender, EventArgs e)
        {
            Ch = true;
        }

        private void secondpause_TextChanged(object sender, EventArgs e)
        {
            Ch = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog DirDialog = new FolderBrowserDialog();
            DirDialog.Description = "Выбор директории";
            DirDialog.SelectedPath = @"C:\";
            DirDialog.ShowDialog();

            dirPath.Text = DirDialog.SelectedPath;
            Properties.Settings.Default.DIRPATH = DirDialog.SelectedPath;
            Properties.Settings.Default.Save();
        }
    }
}
